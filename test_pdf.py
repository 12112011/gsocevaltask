import lhapdf
import numpy as np

def test_pdf():
    pdf = lhapdf.mkPDF("NNPDF40_nnlo_as_01180")
    id = 1  # parton ID
    #edge case tests
    x = 1.0  # edge case where x=1
    Q = 1.0  # edge case where Q=1
    assert np.isclose(pdf.xfxQ(id, x, Q), 0.0)
    assert np.isclose(pdf.xfxQ(id, x, Q), 0.0, rtol=1e-6)